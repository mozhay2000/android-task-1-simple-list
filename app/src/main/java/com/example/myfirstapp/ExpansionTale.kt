package com.example.myfirstapp

data class ExpansionTale(
    val url: String,
    val mainText: String,
    val commsCnt: Int
)

