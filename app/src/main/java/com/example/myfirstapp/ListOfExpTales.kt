package com.example.myfirstapp

class ListOfExpTales {
    fun expansionTales(): List<ExpansionTale> {
        return listOf(
            ExpansionTale(url = "https://qna.habr.com/q/458730", mainText = "User Name 1", commsCnt = 18),
            ExpansionTale(url = "https://metanit.com/java/android/4.9.php", mainText = "text 2", commsCnt = 1008),
            ExpansionTale(url = "https://stackoverflow.com/questions/2201917/how-can-i-open-a-url-in-androids-web-browser-from-my-application", mainText = "User Name 3", commsCnt = 18),
            ExpansionTale(url = "http://www.google.com", mainText = "text3", commsCnt = 2240),
            ExpansionTale(url = "http://www.google.com", mainText = "User Name 5", commsCnt = -20),
            ExpansionTale(url = "http://www.google.com", mainText = "User Name 6", commsCnt = 43),
            ExpansionTale(url = "http://www.google.com", mainText = "User Name 7", commsCnt = 53),
            ExpansionTale(url = "http://www.google.com", mainText = "User Name 8", commsCnt = 2),
        )
    }
}