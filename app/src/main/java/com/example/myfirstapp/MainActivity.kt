package com.example.myfirstapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val recyclerView = findViewById<RecyclerView>(R.id.usersRecyclerView)
        val expansionTaleAdapter = ExpansionTaleAdapter()
        expansionTaleAdapter.users = ListOfExpTales().expansionTales()
        recyclerView.adapter = expansionTaleAdapter
        expansionTaleAdapter.notifyDataSetChanged()

    }
}