package com.example.myfirstapp

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView



class ExpansionTaleAdapter : RecyclerView.Adapter<ExpansionTaleAdapter.ViewHolder>() {

    var users: List<ExpansionTale> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = users[position]

        holder.mainTextView.text = user.mainText
        holder.commsTextView.text = holder.urlButton.context.getString(R.string.comms, user.commsCnt)

        holder.commsTextView.visibility = View.INVISIBLE
        holder.urlButton.visibility = View.INVISIBLE

        holder.showHideButton.setOnClickListener {
            fun View.toggleVisibility() {
                visibility = if (visibility == View.VISIBLE) {
                    View.INVISIBLE
                } else {
                    View.VISIBLE
                }
            }

            holder.commsTextView.toggleVisibility()
            holder.urlButton.toggleVisibility()
        }

        holder.urlButton.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(user.url))
            holder.urlButton.context.startActivity(browserIntent)
        }
    }

    override fun getItemCount(): Int {
        return users.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mainTextView = itemView.findViewById<TextView>(R.id.mainTextView)!!
        val commsTextView = itemView.findViewById<TextView>(R.id.commsCntTextView)!!

        val urlButton = itemView.findViewById<ImageButton>(R.id.urlImageButton)!!
        val showHideButton = itemView.findViewById<ImageButton>(R.id.showHideImageButton)!!
    }

}